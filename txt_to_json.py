import json
def txt_json(txt_file, json_file):
	with open(txt_file,"r") as file:
		data_txt = file.read()
	data_txt_list = data_txt.split("\n")
	data_txt_list.remove("")

	data_dict = {}
	for i in data_txt_list:
		i_dict = {i: i}
		data_dict.update(i_dict)
	json_data = json.dumps(data_dict,indent= 4)
	with open(json_file, "w") as file:
		file.write(json_data)
def main():
	txt_json()
if __name__ == "__main__":
	main()