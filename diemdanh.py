import json


class Student():
    def __init__(self, name, email, your_class):
        self.name = name
        self.email = email
        self.your_class = your_class


def user_input():
    name = input("enter name: ")
    email = input("enter mail:")
    your_class = input("enter class: ")
    infor = Student(name, email, your_class)
    return infor


def make_json_file():
    info_dict = {
        "infor": [
        ]
    }
    with open("data.json", "w") as f:
        json.dump(info_dict, f, indent=4)


def email_data(infor):
    empty_dict = {
        infor.name: {
            "mail": infor.email,
            "class": infor.your_class,
        }
    }
    # json_list = json.dumps(empty_dict, indent=4)
    # info_list.append(empty_dict)
    return empty_dict


def write_email_to_json(empty_dict):
    with open("data.json", "w") as f:
        json.dump(empty_dict, f, indent=4)


def append_email_to_json(info_dict):
    with open("data.json") as f:
        dict1 = json.load(f)
        dict1["infor"].append(info_dict)
        write_email_to_json(dict1)


def main():
    with open("data.json","r") as f:
        if json.load(f) == None:
            make_json_file()

    infor = user_input()
    info_dict = email_data(infor)
    append_email_to_json(info_dict)


if __name__ == "__main__":
    main()

