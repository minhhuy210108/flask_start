from flask import Flask
from txt_to_json import txt_json
import json

app = Flask(__name__)

@app.route('/')
def return_json():
    dict_file = {
        "id": "hello",
        "name": "huy",
        "key": "value"
    }
    json_file = json.dumps(dict_file, indent=4)
    return json_file
if __name__ == "__main__":
    app.run(debug=True)